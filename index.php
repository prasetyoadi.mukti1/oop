<?php


require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$animal = new Animal("shaun");
echo "Name = " . $animal->name . "<br>";
echo "Legs = " . $animal->legs . "<br>";
echo "Cold Blooded = " . $animal->cold_blooded . "<br><br>";

$kodok  = new Frog("buduk");
echo "Name = " . $kodok->name . "<br>";
echo "Legs = " . $kodok->legs . "<br>";
echo "Cold Blooded = " . $kodok->cold_blooded . "<br>";
echo "jump =" . $kodok->jump . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name = " . $sungokong->name . "<br>";
echo "Legs = " . $sungokong->legs . "<br>";
echo "Cold Blooded = " . $sungokong->cold_blooded . "<br>";
echo "yell = " . $sungokong->yell;
